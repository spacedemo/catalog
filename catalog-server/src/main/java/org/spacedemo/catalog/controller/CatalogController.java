package org.spacedemo.catalog.controller;


import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.storage.GoogleStorageResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;

@Controller
public class CatalogController {
	
	@Value("gs://${gcs-bucket}")
	private Resource gcsResource;
	
	public void receivePicture(String id, MultipartFile image) throws IOException {
		GoogleStorageResource resource = ((GoogleStorageResource)gcsResource);
		
		
		Bucket bucket = resource.getBucket();
		
		
		Blob blob = bucket.create(id+".png", image.getInputStream());

		URL signedUrl =blob.signUrl(14, TimeUnit.DAYS);

		System.out.println("TEST 01 = " + signedUrl.toString());
		
		
		
	}
}
