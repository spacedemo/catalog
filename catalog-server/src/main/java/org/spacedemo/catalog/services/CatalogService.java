package org.spacedemo.catalog.services;

import java.io.IOException;

import org.spacedemo.catalog.controller.CatalogController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin(origins = "*")
public class CatalogService {

	@Autowired
	private CatalogController catalogController;

	@PostMapping(value = "/image/{id}", consumes = {"multipart/form-data"})
	public void post(@PathVariable("id") String id, @RequestParam("image") MultipartFile image) throws IOException {
		catalogController.receivePicture(id, image);
	}

}
