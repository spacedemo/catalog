FROM openjdk:8-jre-slim

COPY catalog-server/target/catalog-app.jar .

CMD ["java", "-jar", "catalog-app.jar"]
